# include <queue>
# include <vector>
# include <string>
# include <algorithm>
# include <ctime>
# include <iostream>
# include <fstream>
# include <sstream>
# include <cmath>

# include <stdlib.h>
# include <time.h>

# include <Rcpp.h>

# define DD_STEPSIZE 2
# define DD_ITER 200
# define DD_EPS 1e-5
# define DD_CHANGE 1.5
# define PMAX 10000

using namespace Rcpp;
using namespace std;

struct RANK_SCORE
{
  double value;
  int index;
};

struct rr
{ 
  int a;
  int b;
  double c;
};

bool mycmpcmp(const rr &x, const rr &y)
{ 
  return fabs(x.c) > fabs(y.c);
}

bool cmp_score(const RANK_SCORE &a, const RANK_SCORE &b)
{
  return (a.value > b.value);
}

bool cmp_absolute_score(const RANK_SCORE &a, const RANK_SCORE &b)
{
  return (fabs(a.value) > fabs(b.value));
}

void GENP(int n, double gamma, int *g)
{
  double *STORE;
  double sum = 0;
  double nodesum = 0;
  int index = 1;
  STORE = new double [PMAX+1];

  for (int i=1; i<=PMAX; ++i)
    {
      STORE[i] = pow(1.0/i,gamma);
    }
  for (int i=PMAX; i>0; --i)
    {
      sum += STORE[i];
    }

  for (int i=PMAX; i>0; --i)
    {

      nodesum += STORE[i];

      while (nodesum >= sum/n)
        {
          g[index] = i;
          ++index;
          nodesum -= sum/n;
        }

      if (index > n)
        {
          break;
        }
    }

  if (nodesum > 0)
    {
      g[n] = 1;
    }

  delete []STORE;
}

void GENH(int n, double *h)
{
  for (int i=1; i<n; ++i)
    {
      h[i] = log(i+1)-log(i);
    }
}

void TH(int n, double *th)
{
  for (int i=1; i<n+1; ++i)
    {
      th[i] = pow(log(i+1),0.09);
    }
}

void TG(int n, int *g, double *th, double *tg)
{
  for (int i=1; i<=n; ++i)
    {
      tg[n+1-i] = 1.0/th[(int) (g[i]*1.4)];
    }
}

void CAL_SCORE(int n, double **M, double *h, struct RANK_SCORE *sort_score)
{
  priority_queue <double> pq;

  for (int i=1; i<n+1; ++i)
    {
      for (int j=1; j<n+1; ++j)
        {
          if (i != j)
            {
              pq.push(fabs(M[i][j]));
            }
        }

      sort_score[i].value = 0;
      sort_score[i].index = i;

      for (int j=1; j<n; ++j)
        {
          sort_score[i].value += pq.top()*h[j];
          pq.pop();
        }
    }
}

void ROW_SCORE(int n, int id, double *myans, double *M, double *h, double rho, double alpha, struct RANK_SCORE *myscore, double *old_h)
{
  alpha /= rho;

  int len;
  int *sumlen;
  double *mysum;
  double *ans;
  struct RANK_SCORE *myM;

  sumlen = new int[n+2];
  mysum = new double[n+2];
  ans = new double[n+2];
  myM = new RANK_SCORE[n+2];

  sumlen[0] = 0;
  mysum[0] = 0;
  len = 0;
  for (int i=1; i<id; ++i)
    { 
      myM[i].value = M[i];
      myM[i].index = i;
    }

  for (int i=id+1; i<n+1; ++i)
    { 
      myM[i-1].value = M[i];
      myM[i-1].index = i;
    }

  sort(myM+1,myM+n,cmp_absolute_score);

  for (int i=1; i<n; ++i)
    {
      mysum[i] = mysum[i-1] + fabs(myM[i].value)-h[i]*alpha;
      int left = 1;
      int right = len;
      int mid;

      while (right >= left)
        {
          mid = (left+right)/2;

          double tmp = ans[mid]*(i-sumlen[mid-1])-(-mysum[sumlen[mid-1]]+mysum[i]);

          if (tmp == 0 || ans[mid] == 0)
            {
              break;
            }
          else if (tmp > 0)
            {
              left = mid+1;
            }
          else
            {
              right = mid-1;
            }
        }

      if (left > right)
        {
          len = right+1;
          sumlen[len] = i;
          ans[len] = (mysum[i]-mysum[sumlen[len-1]]>0)?(mysum[i]-mysum[sumlen[len-1]]):0;
          ans[len] /= (i-sumlen[len-1]);
        }
      else
        {
          len = mid;
          ans[mid] = (mysum[i]-mysum[sumlen[mid-1]]>0)?(mysum[i]-mysum[sumlen[mid-1]]):0;
          sumlen[mid] = i;
          ans[mid] /= (i-sumlen[mid-1]);
        }
    }

  myscore[id].value = 0;
  myscore[id].index = id;
  
  for (int i=1; i<=len; ++i)
    {
      for (int j=sumlen[i-1]+1; j<=sumlen[i]; ++j)
        {
          if (myM[j].value > 0)
            {
              myans[myM[j].index] = ans[i];
            }
          else
            {
              myans[myM[j].index] = 0-ans[i];
            }

          myscore[id].value += ans[i]*old_h[j];
        }
    }

  myans[id] = M[id];

  delete []sumlen;
  delete []mysum;
  delete []myM;
  delete []ans;
}

void NON_SYMMETRIC_OPT(int n, double **myans, double **M, double *h, double *th, double *g, double rho, double alpha, double beta, struct RANK_SCORE *myscore)
{
  double **tmph;
  tmph = new double *[n+1];
  for (int i=0; i<n+1; ++i)
    {
      tmph[i] = new double [n+1];
    }

  for (int i=1; i<n+1; ++i)
    {
      for (int j=1; j<n; ++j)
        {
          tmph[i][j] = alpha*h[j] + beta*g[i]*th[j];
        }

      ROW_SCORE(n,i,myans[i],M[i],tmph[i],rho,1,myscore,th);
    }

  for (int i=0; i<n+1; ++i)
    {
      delete [] tmph[i];
    }
  delete []tmph;
}

void DD(int n, double **myans, double **M, double *ph, double *h, double *g, double rho, double alpha, double theta, struct RANK_SCORE *myscore)
{
  int **check_sign;
  double **step_size;
  double **beta;
  bool break_sign;

  check_sign = new int *[n+1];
  step_size = new double *[n+1];
  beta = new double *[n+1];
  for (int i=0; i<n+1; ++i)
    {
      beta[i] = new double [n+1];
      step_size[i] = new double [n+1];
      check_sign[i] = new int [n+1];
    }

  for (int i=0; i<n+1; ++i)
    {
      for (int j=0; j<n+1; ++j)
        {
          beta[i][j] = M[i][j];
          step_size[i][j] = DD_STEPSIZE;
          check_sign[i][j] = 0;
        }
    }

  for (int i=0; i<DD_ITER; ++i)
    {
      break_sign = true;

      NON_SYMMETRIC_OPT(n, myans, beta, ph, h, g, rho, alpha, theta, myscore);
      for (int j=1; j<n+1; ++j)
        {
          for (int k=j; k<n+1; ++k)
            {
              if (fabs(myans[j][k]-myans[k][j]) > DD_EPS)
                {
                  break_sign = false;
                }

              if ((myans[j][k]-myans[k][j])*check_sign[j][k] > 0)
                {
                  step_size[j][k] = step_size[j][k]*DD_CHANGE;
                }
              else if ((myans[j][k]-myans[k][j])*check_sign[j][k] < 0)
                {
                  step_size[j][k] = step_size[j][k]/DD_CHANGE;
                }

              if (myans[j][k] - myans[k][j] > 0)
                {
                  check_sign[j][k] = 1;
                }
              else if (myans[j][k] - myans[k][j] < 0)
                {
                  check_sign[j][k] = -1;
                }
              else
                { 
                  check_sign[j][k] = 0;
                }
              beta[j][k] -= 2*step_size[j][k]/rho*(myans[j][k]-myans[k][j]);
              beta[k][j] += 2*step_size[j][k]/rho*(myans[j][k]-myans[k][j]);
            }
        }

      if (break_sign)
        {
          break;
        }
    }

  for (int i=1; i<n+1; ++i)
    {
      for (int j=i; j<n+1; ++j)
        {
          myans[i][j] = myans[j][i] = (myans[i][j]+myans[j][i])/2;
        }
    }

  for (int i=0; i<n+1; ++i)
    {
      delete [] check_sign[i];
      delete [] step_size[i];
      delete [] beta[i];
    }
  delete []check_sign;
  delete []step_size;
  delete []beta;
}

void ADJUST(int n, double **myans, double **M, double *h, double *th, double *g, double rho, double alpha, double theta)
{
  double asize = 0.5;
  int alimit = 200;
  srand(time(NULL));

  struct RANK_SCORE *sort_score;
  double *beta;
  double *penaltyg;
  double *nodeg;

  double *mysize;
  double *mydiff;

  sort_score = new struct RANK_SCORE [n+2];
  penaltyg = new double [n+2];
  nodeg = new double [n+2];
  beta = new double [n+2];
  mysize = new double [n+2];
  mydiff = new double [n+2];

  for (int i=1; i<n+1; ++i)
    { 
      beta[i] = 0;
      mydiff[i] = 0;
      mysize[i] = asize;
    }

  CAL_SCORE(n, M, h, sort_score);
  sort(sort_score+1, sort_score+n+1, cmp_score);
  for (int i=1; i<n+1; ++i)
    { 
      penaltyg[sort_score[i].index] = g[n-i+1];
    }

  bool pre_sign = false;
  bool curr_sign;

  for (int iter = 0; iter < alimit; ++iter)
    {
      curr_sign = true;

      DD(n, myans, M, h, th, penaltyg, rho, alpha, theta, sort_score);

      for (int i=1; i<n+1; ++i)
        { 
          sort_score[i].value += beta[i];
        }
      sort(sort_score+1, sort_score+n+1, cmp_score);
      for (int i=1; i<n+1; ++i)
        { 
          penaltyg[sort_score[i].index] = g[n-i+1];
        }
      CAL_SCORE(n, myans, h, sort_score);
      sort(sort_score+1, sort_score+n+1, cmp_score);
      for (int i=1; i<n+1; ++i)
        {
          nodeg[sort_score[i].index] = g[n-i+1];
        }
      for (int i=1; i<n+1; ++i)
        { 
          if ((penaltyg[i] - nodeg[i])*mydiff[i] > 0)
            { 
              mysize[i] *= 1.5*(0.7+rand()%1000*1.0/1000);
            }
          else if ((penaltyg[i] - nodeg[i])*mydiff[i] < 0)
            { 
              mysize[i] /= 1.5*(0.7+rand()%1000*1.0/1000);
            }

          beta[i] += mysize[i]*(penaltyg[i]-nodeg[i]);
          mydiff[i] = penaltyg[i] - nodeg[i];

          if (penaltyg[i] != nodeg[i])
            { 
              curr_sign = false;
            }
        }

      if (curr_sign && pre_sign)
        {
          break;
        }

      pre_sign = curr_sign;
    }

  delete []sort_score;
  delete []beta;
  delete []penaltyg;
  delete []nodeg;
  delete []mysize;
  delete []mydiff;
}


//siqi

// [[Rcpp::export]]
NumericMatrix Lovasz(NumericMatrix MM, double gamma, double rho, double alpha, double beta)
{
	int n = MM.nrow(); // Size of matrix
	
	double *h; // Lovasz extension
	int *g; // Discrete power law
	double *tg;
	double *th;
	double **ans; // Result
	double **M; // Input M
	
	bool symmetric = false;
	
	h = new double[n+1];
	g = new int[n+1];
	th = new double[n+1];
	tg = new double[n+1];
	M = new double* [n+1];
	ans = new double* [n+1];
	
	for (int i=0; i<n+1; ++i)
	{
		ans[i] = new double[n+1];
		M[i] = new double[n+1];
	}
	GENH(n,h);
	GENP(n,gamma,g);
	TH(n,th);
	TG(n,g,th,tg);

	for (int i=1; i<n+1; ++i)
		for (int j=1; j<n+1; ++j)
			M[i][j] = MM(i-1, j-1);
	
	ADJUST(n, ans, M, h, th, tg, rho, alpha, beta);
	
	NumericMatrix res(n, n);
	for (int i=1; i<n+1; ++i)
		for (int j=1; j<n+1; ++j)
			res(i-1,j-1) = ans[i][j];	

	for (int i=0; i<n+1; ++i)
	  {
	    delete []ans[i];
	    delete []M[i];
	  }

	delete []h;
	delete []g;
	delete []tg;
	delete []th;
	delete []ans;
	delete []M;

	return res;
}



struct record
{
	double a;
	double b;
};

bool mycmp2(const record &x, const record &y)
{
	return x.a > y.a;
}

bool mycmp1(double a, double b)
{
	return a>b;
}

// [[Rcpp::export]]
double ComputeObj2(NumericMatrix M, NumericMatrix X, double gamma, double rho, double alpha, double beta)
{
	int n = M.ncol();
	double value = 0;

	double *h;
	int *g;
	double *th;
	double *tg;
	double **s;	
	
	g = new int[n+1];
	h = new double[n+1];
	tg = new double[n+1];
	th = new double[n+1];
	s = new double *[n+1];

	for (int i=0; i<n+1; ++i)
	  {
	    s[i] = new double [n+1];
	  }

	GENH(n,h);
	GENP(n,gamma,g);
	TH(n,th);
	TG(n,g,th,tg);
	
	for (int i=0; i<n; ++i)
	  {
	    for (int j=0; j<n; ++j)
	      {
		value += (M(i,j)-X(i,j))*(M(i,j)-X(i,j));
	      }
	  }
	value /= 2;
	value *= rho;

	for (int i=0; i<n; ++i)
	  {
	    for (int j=0; j<i; ++j)
	      {
		s[i][j] = fabs(M(i,j));
	      }

	    for (int j=i+1; j<n; ++j)
	      {
		s[i][j-1] = fabs(M(i,j));
	      }

	    sort(s[i],s[i]+n-1,mycmp1);
	  }
  
	struct RANK_SCORE r[n+2];

	for (int i=0; i<n; ++i)
	  {
	    r[i].value = 0;
	    r[i].index = i;

	    for (int k=0; k<n-1; ++k)
	      {
		r[i].value += s[i][k]*h[k+1];
	      }
	    value += alpha*r[i].value;
	  }
  
	sort(r,r+n,cmp_score);
  
	for (int i=0; i<n; ++i)
	  {
	    int id = r[i].index;

	    for (int k=0; k<n-1; ++k)
	      {
		value += beta*tg[n-i]*s[id][k]*th[k+1];
	      }
	  }

	for (int i=0; i<n+1; ++i)
	  {
	    delete [] s[i];
	  }

	delete []h;
	delete []g;
	delete []th;
	delete []tg;
	delete []s;

	return value;
}

struct node
{
	int i;
	int j;
	double val;
	node(int a, int b, double c):i(a), j(b), val(c){}
	node(){i=0; j=0; val=0;}
};

class compare
{
	public:
	bool operator() (const node &n1, const node &n2) const
	{return n1.val > n2.val;}
};

// [[Rcpp::export]]
NumericMatrix Predict(NumericMatrix X, int n_edge)
{
	priority_queue<node, vector<node>, compare> h;
	
	int count = 0;
	int n = X.ncol();
	for(int i=0; i<n; ++i)
	{
		for(int j=i+1; j<n; ++j)
		{
			if(h.size()< static_cast<unsigned int>(n_edge) )
			h.push( node(i+1, j+1, X(i,j) ) );
			else
			{
				if(X(i,j) > h.top().val)
				{
					h.pop();
					h.push( node(i+1, j+1, X(i,j) ) );
				}
			}
			//cout << h.top().i << "\t" << h.top().j << "\t" << h.top().val << endl;
			++count;
		}
	}	
	NumericMatrix res(n_edge, 3);
	count = 0;
	while(!h.empty())
	{
		node tmp = h.top();
		res(count,0) = tmp.i;
		res(count,1) = tmp.j;
		res(count,2) = tmp.val;
		h.pop();
		++count;
	}
	return res;
}

// [[Rcpp::export]]
NumericVector ValidateRes(NumericMatrix pred, NumericMatrix X)
{
	int n = pred.nrow();
	NumericVector res(n);
	for(int i=0; i<n; ++i)
	{
		if( X( pred(i,0), pred(i,1) ) > 0)
		res(i) = 1;
		else
		res(i) = 0;
	}
	return res;
}

// [[Rcpp::export]]
void RANK(NumericMatrix Precision)
{ 
  int n = Precision.nrow();
  int index = 0;
  struct rr *tmp;
  tmp = new struct rr [n*n];

  for (int i=0; i<n; ++i)
    { 
      for (int j=i+1; j<n; ++j)
        { 
          tmp[index].a = i;
          tmp[index].b = j;
          tmp[index++].c = Precision(i,j);
        }
    }

  sort(tmp,tmp+index,mycmpcmp);

  cout<<"START RANKING"<<endl;

  for (int i=0; i<n*(n-1)/2; ++i)
    { 
      if (fabs(tmp[i].c) < 1e-6 )
	{
	  break;
	}

      cout<<tmp[i].a<<" "<<tmp[i].b<<" "<<tmp[i].c<<endl;
    }

  delete [] tmp;

  return;
}
