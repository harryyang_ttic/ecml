struct rr
{
  int a;
  int b;
  double c;
};

bool mycmp(struct rr &x, struct rr &y)
{
  return x.c > y.c;
}

// [[Rcpp::export]]
void RANK(NumericMatrix Precision)
{
  int n = Precision.nrow();
  int index = 0;
  struct rr *tmp;
  tmp = new struct record [n*n];

  for (int i=0; i<n; ++i)
    {
      for (int j=i+1; j<n; ++j)
	{
	  tmp[index].a = i;
	  tmp[index].b = j;
	  tmp[index++].c = Precision(i,j);
	}
    }

  sort(tmp,tmp+index,mycmp);

  for (int i=0; i<5000; ++i)
    {
      out<<tmp[i].a<<" "<<tmp[i].b<<endl;
    }
}
