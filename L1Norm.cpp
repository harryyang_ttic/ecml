# include <cmath>
# include <iostream>
# include <Rcpp.h>

using namespace Rcpp;
using namespace std;

//[[Rcpp::export]]
double L1_NORM(NumericMatrix X, NumericMatrix Y, int iter)
{
  int size1 = X.nrow();
  int size2 = Y.ncol();
  double error = 0;

  for (int i=0; i<size1; ++i)
    {
      for (int j=0; j<size2; ++j)
	{
	  error += fabs(X(i,j) - Y(i,j));
	}
    }

  cout<<iter<<" "<<error<<endl;

  return error;
}
